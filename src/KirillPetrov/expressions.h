#ifndef _EXPRESSIONS_H_
#define _EXPRESSIONS_H_


#include <iostream>

using std::string;


#define MAX 255

string Transfer (const string &);
float Computation (const string &);
int ControllingParentheses (const string &, bool);


#endif